import Vue from 'vue';
import App from './App.vue';
import StartMenu from './components/StartMenu.vue';
import EndScreen from "./components/EndScreen"
import Question from './components/Question';
import VueRouter from 'vue-router';
import 'bulma/css/bulma.css';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faHome } from '@fortawesome/free-solid-svg-icons'

Vue.component('font-awesome-icon', FontAwesomeIcon)
library.add(faHome)
Vue.use(VueRouter);

const routes = [{
        path: "/question",
        name: "Question",
        component: Question
    },
    {
        path: "/",
        name: "StartMenu",
        component: StartMenu,
    },
    {
        path: "/endscreen",
        name: "EndScreen",
        component: EndScreen,
    },
];

const router = new VueRouter({ routes });
new Vue({
        el: "#app",
        router,
        render: h => h(App)
    }) //.$mount('#app');